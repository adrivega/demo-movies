package com.example.demo.Controller;

import com.example.demo.Model.Movie;
import com.example.demo.service.MovieService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

@Controller
public class BasicController {
    @Autowired
    private MovieService movieService;
    @RequestMapping("/")
    String hola(){
            return "Index";
    }



    @RequestMapping("/add")
    String add(){
        return "addMovie";
    }
    @PostMapping("/add")
    String prueba1(@RequestParam String movie_name, @RequestParam String url, Model model){
        Movie m = new Movie();
        m.setUrl(url);
        m.setMovie_name(movie_name);

        movieService.addMovie(m);

        model.addAttribute("message", "La película '" + movie_name + "' ha sido añadida.");

        return "Index";
    }



    @RequestMapping("/delete")
    String delete(){
        return "deleteMovie";
    }
    @DeleteMapping("/delete")
    String prueba2(@RequestParam Long id, Model model) {
        movieService.deleteMovie(id);
        //ya hemos borrado la peli

        model.addAttribute("message", "La película '" + id + "' ha sido borrada.");

        return "index";
    }



    @RequestMapping("/update")
    String update(){
        return "updateMovie";
    }
    @PutMapping("/update")
    String prueba3(@RequestParam String movie_name, @RequestParam String url, @RequestParam Long movie_id, Model model) {

        Movie u = new Movie();
        u.setUrl(url);
        u.setMovie_name(movie_name);

        movieService.updateMovie(movie_id, u);

        model.addAttribute("message", "La película '" + movie_name + "' ha sido editada.");

        return "index";
    }



}
